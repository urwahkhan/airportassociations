/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airport;

/**
 *
 * @author urwah
 */
public class Airplane {
    private int id;
    private int numOfSeats;
    private String model;
    private String  make;
    
    public Airplane(int id, int numOfSeats,String model,String make){
        this.id=id;
        this.numOfSeats=numOfSeats;
        this.model=model;
        this.make=make;
        System.out.println("Plane id:"+ id);
        System.out.println("Plane Seats:"+ numOfSeats);
       System.out.println("Plane model:"+ model);
       System.out.println("Plane make:"+ make);


    }
    
    public String getModel(){
        return this.model;
    }
    
    public String getMake(){
        return this.make;
    }
    
    public int getId() {
        return this.id;
    }
    public int getnumOfSeats(){
        return this.numOfSeats;
    }
    
    
    
    
}
